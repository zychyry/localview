# LocalView #

This is a simple universal iOS app written in Objective-C that pulls images from the Flickr API based on the user's current location.

### Included Dependencies ###
* [AFNetworking](https://github.com/AFNetworking/AFNetworking)
* [SDWebImage](https://github.com/rs/SDWebImage)